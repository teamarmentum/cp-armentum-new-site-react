import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './responsive.scss';
import Views from './views/Views';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Views />, document.getElementById('root'));
registerServiceWorker();
