import React from "react";
import "../../index.css";
import $ from "jquery";
class Objectives extends React.Component {
	onPress = e => {
		e.preventDefault();
		var t = { scrollTop: $(e.currentTarget.hash).offset().top + 60 };
		$("html, body").animate(t, t.scrollTop / 1.5);
	};
	render() {
		return (
			<div id="objId">
				<div className="container container_space">
					<section className="obj-1" id="objectives" onPress={this.onPress}>
						<div className="row row-obj">
							<div className="obj-head">
								<span className="head-color-red">HOW WE WORK</span>
								<span className="head-color-blk"> : OSCAR</span>
								<p className="objectives">Objectives</p>
							</div>
						</div>
						<div className="row medium-row">
							<div className="left-section col-md-6 col-sm-6">
								<p className="content1">
									When setting objectives, we limit it to three objectives and
									ensure that they are quantitative, measurable, and time bound.
									Depending on the project, we set different types of
									objectives. For digital products that are nascent, we focus on
									user acquisition and churn and for applications that already
									have users, we focus on long-term value (LTV).
								</p>
							</div>
							<div className="right-section col-md-6 col-sm-6">
								<img
									src="/Front-images/objective-img-1.png"
									className="right-img-obj1"
									alt="objective"
								/>
							</div>
						</div>
					</section>
					<div className="seperator-1">
						<img
							src="/Front-images/arrow-left.png"
							className="left-arrow-1"
							alt="left arrow"
						/>
					</div>
					<section className="obj-2" id="strategy" onPress={this.onPress}>
						<div className="row row-obj1">
							<div className="obj-head1">
								<p className="strategy">Strategy</p>
							</div>
						</div>
						<div className="row medium-row1">
							<div className="left-section1 col-md-6 col-sm-6">
								<img
									src="/Front-images/img-visual-strategy-new.png"
									className="left-img-obj1"
									alt="objective"
								/>
							</div>
							<div className="right-section1 col-md-6 col-sm-6">
								<p className="content2">
									We begin the OSCAR Methodology strategy phase by performing a
									360° Analysis of your product and of the existing marketing
									channels that are in play. This analysis typically begins with
									reviewing on-site analytics from services such as Hotjar.com
									and/or Google Analytics. We conduct on-page analysis, map the
									visitor journey, and understand where visitors are coming
									from. To dig deeper, we perform a Cohort Analysis to further
									understand the user behaviors of different sets of users.
								</p>
							</div>
						</div>
					</section>
					<div className="seperator-2">
						<img
							src="/Front-images/arrow-right.png"
							className="right-arrow-1"
							alt="right arrow"
						/>
					</div>
					<section className="obj-3" id="create" onPress={this.onPress}>
						<div className="row row-obj2">
							<div className="obj-head2">
								<p className="create">Create</p>
							</div>
						</div>
						<div className="row medium-row2">
							<div className="left-section2 col-md-6 col-sm-6">
								<p className="content3">
									At this OSCAR Methodology phase, we create the Content
									Guidelines &amp; Architecture and send it over to the content
									team (or client) to begin writing the content. The
									Notification Triggers are also finalized at the end of this
									phase. This Phase, like the previous one, requires constant
									communicate with the client to move rapidly through different
									iterations. We use Invision to decrease the feedback loop
									cycle and for a-synchronous communication.
								</p>
							</div>
							<div className="right-section2 col-md-6 col-sm-6">
								<img
									src="/Front-images/objective-img-3.png"
									className="right-img-obj2"
									alt="objective"
								/>
							</div>
						</div>
					</section>
					<div className="seperator-3">
						<img
							src="/Front-images/arrow-left.png"
							className="left-arrow-2"
							alt="left arrow"
						/>
					</div>
					<section className="obj-4" id="analysis" onPress={this.onPress}>
						<div className="row row-obj3">
							<div className="obj-head3">
								<p className="strategy">Analysis</p>
							</div>
						</div>
						<div className="row medium-row3">
							<div className="left-section3 col-md-6 col-sm-6">
								<img
									src="/Front-images/objective-img-4.png"
									className="left-img-obj2"
									alt="objective"
								/>
							</div>
							<div className="right-section3 col-md-6 col-sm-6">
								<p className="content4">
									Every month, we perform a 360° Audit on the Traffic Generation
									Engine that we have built. We begin by comparing the overall
									goals set vs. actual results and also perform a deeper audit
									by understanding performance for each channel. Because we have
									already set up tracking systems, running these reports is the
									easy part.
								</p>
							</div>
						</div>
					</section>
					<div className="seperator-4">
						<img
							src="/Front-images/arrow-right.png"
							className="right-arrow-2"
							alt="right arrow"
						/>
					</div>
					<section className="obj-5" id="refinement" onPress={this.onPress}>
						<div className="row row-obj4">
							<div className="obj-head4">
								<p className="refinement">Refinement</p>
							</div>
						</div>
						<div className="row medium-row4">
							<div className="left-section4 col-md-6 col-sm-6">
								<p className="content5">
									Based on the recommendations from the analysis, our team
									optimizes the components of the revenue engine. This includes
									updates to the website/app design based on the A/B testing
									results to optimize the messaging in the different marketing
									channels. We also add new marketing channels depending on our
									bandwidth and also some remove ones that are poorly converting
									and not worth the time or investment. We typically limit
									update cycles to 2 weeks and push out the new updates within
									that time-frame. For existing channels, we continue to product
									and publish content according to the Content Calendar.
								</p>
							</div>
							<div className="right-section4 col-md-6 col-sm-6">
								<img
									src="/Front-images/objective-img-5.png"
									className="right-img-obj5"
									alt="objectives"
								/>
							</div>
						</div>
					</section>
				</div>
			</div>
		);
	}
}

export default Objectives;
