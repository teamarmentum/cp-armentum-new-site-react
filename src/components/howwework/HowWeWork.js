import React, { Component } from "react";
import Oscarimages from "./Oscarimages";
import Navbar from "../../components/Page-2/navbar/Navbar";
import "../../components/style.scss";
import "./style.scss";

export default class HowWeWork extends Component {
  render() {
    return <div className="page how-we-work" id="id1" style={{ display: "block", height: window.innerWidth > 768 ? window.innerHeight : "auto", position: "static" }}>
        <div className="container">
          <div className="row page3-row" style={{ transform: "translate(-35px,30px)" }}>
            <div className="col-2 topspace" style={{ alignItems: "center" }}>
              <a href="Homepage" style={{ paddingTop: "10px" }}>
                <img src="/Front-images/logo.png" alt="Armentum Logo" className="logo" style={{ width: 383, height: 62 }} />
              </a>
            </div>
          </div>
          <div className="row leftSpace" style={{ flex: 2 }}>
            <div className="col" style={{ display: "flex", alignItems: "center" }}>
              <div className="spacing">
                <h1 className="heading-1">
                  <span style={{ color: "#d94645" }}>H</span>ow we work
                </h1>
                <h3 className="heading-3">
                  Armentum has developed <br />Oscar Methodology to win online.
                </h3>

                <a className="teambtn" onClick={() => {
                    this.props.setScreenNext(5);
                  }}>
                  <button className="button" style={{ marginTop: "80px" }}>
                    TALK TO OUR EXPERTS
                  </button>
                </a>
              </div>
            </div>
            <div className="col" style={{ display: "flex", alignItems: "center", background: "url('/Front-images/oscar-circle.png')", backgroundSize: "90% 90%", backgroundRepeat: "no-repeat", backgroundPosition: "129px 25px" }}>
              <Oscarimages />
              {/*<img src="/Front-images/OSCAR-1-pic.png" className="oscar-content" alt="Oscar" usemap="#planetmap"/>
              <map name="planetmap">
                <area shape="rect" coords="100,100,82,126" alt="obj1" href="sun.htm"/>
                <area shape="circle" coords="90,58,3" alt="Mercury" href="mercur.htm"/>
                <area shape="circle" coords="124,58,8" alt="Venus" href="venus.htm"/>
              </map>*/}
              {/*<Navbar style={{right:"5%", top:"0%"}}/>*/}
            </div>
          </div>
        </div>
      </div>;
  }
}
