import React, { Component } from "react";
import $ from "jquery";
export default class Oscarimages extends Component {
  onPress = e => {
    e.preventDefault();
    // $('a[href^="#"]').bind(function(){
    // var the_id = $(this).attr("href");
    var t = { scrollTop: $(e.currentTarget.hash).offset().top + 30 };
    $("html, body").animate(t, t.scrollTop / 1.5);

    // return false;});
  };
  render() {
    return (
      <div className="oscarimages" style={this.props.style}>
        <div className="oscarimage1">
          <a href="#objectives" onClick={this.onPress}>
            <img
              src="/Front-images/objectives1.png"
              alt="Planning"
              className="image1"
            />
          </a>
          <div className="hidden-img">
            <img
              src="/Front-images/o-img.png"
              alt="Planning"
              className="image6"
            />
          </div>
        </div>
        <div className="oscarimage2">
          <a href="#strategy" onClick={this.onPress}>
            <img
              src="/Front-images/objectives2.png"
              alt="Planning"
              className="image2"
            />
          </a>
        </div>
        <div className="oscarimage3">
          <a href="#create" onClick={this.onPress}>
            <img
              src="/Front-images/objectives3.png"
              alt="Planning"
              className="image3"
            />
          </a>
        </div>
        <div className="oscarimage4">
          <a href="#analysis" onClick={this.onPress}>
            <img
              src="/Front-images/objectives4.png"
              alt="Planning"
              className="image4"
            />
          </a>
        </div>
        <div className="oscarimage5">
          <a href="#refinement" onClick={this.onPress}>
            <img
              src="/Front-images/objectives5.png"
              alt="Planning"
              className="image5"
            />
          </a>
        </div>
      </div>
    );
  }
}
