import React, { Component } from "react";

import HowWeWork from "./HowWeWork";
import BottomFooter from "../../components/bottomFooter/index";
import Layout9 from "../../views/Layout9";

import "../../components/style.scss";
import "./style.scss";

export default class Howwework extends Component {
  render() {
    return (
      <div className="parallax">
        <div className="parallax__layer parallax__layer--back">
          <HowWeWork setScreenNext={this.props.setScreenNext} />
          <div className="child">
            <Layout9 />
          </div>
        </div>
          {/* <BottomFooter/> */}
      </div>
    );
  }
}
