import React, { Component } from "react";
import "../../components/style.scss";
import "./style.scss";

export default class BottomFooter extends Component {
  render() {
    return (<div className="page" id="homepage">
        <div className="container centerit">
          <div className="row" style={{ transform: "translate(-35px,20px)" }}>
            <div className="col-2" style={{ alignItems: "center" }}>
              <a href="Homepage" style={{ paddingTop: "10px" }}>
                <img src="/Front-images/logo.png" alt="Armentum Logo" style={{ width: 240, height: 40 }} />
              </a>
            </div>
            {/* <div>
              <img
                src="/Front-images/nav-icon.png"
                alt="Nav Icon"
                style={{ width: 50 }}
              />
            </div> */}
          </div>
          <div className="row row-2" style={{ flex: 2 }}>
            <div className="col column-1" style={{ display: "flex", alignItems: "center" }}>
                <div className="head1">Join our Team</div>
                <hr />
                <div className="head2">View CURRENT OPENINGS</div>
                <hr />
                <div className="head3">CASE STUDY</div>
                <hr />
                <div className="head4">WRITE US: info@armentum.co</div>
            </div>
            <div className="col column-2" style={{ display: "flex", alignItems: "center" }}>
              <div className="row row-2" style={{ flex: 2 }}>
                <div className="col column-2" style={{ display: "flex", alignItems: "center" }}>
                    <div className="head1">Quick Links</div>
                    <hr />
                    <div className="head2">HOME</div>
                    <hr />
                    <div className="head3">WHAT WE DO </div>
                    <hr />
                    <div className="head4">HOW WE WORK</div>
                </div>
                <div className="col column-2" style={{ display: "flex", alignItems: "center" }}>
                    <div className="head1"></div>
                    <hr />
                    <div className="head2">ABOUT US</div>
                    <hr />
                    <div className="head3">TEAM</div>
                    <hr />
                    <div className="head4">CONTACT US</div>
                </div>
            </div>
          </div>
          <div className="col column-1" style={{ display: "flex", alignItems: "center" }}>
                <div className="head1">FOLLOW US</div>
                <hr />
                <div className="head2"><span><img src="" className="bottom-img1"/></span><span><img src="" className="bottom-img2"/></span></div>
                <hr />
                <div className="head4">PRIVACY POLICY | TERMS AND CONDITIONS</div>
          </div>
         </div>
         <div className="row row-2" style={{ flex: 2 }}>
            @ Copyright 2016 - 2018 Armentum.co.All rights reserved.
         </div>
        </div>
      </div>
      );
  }
}
