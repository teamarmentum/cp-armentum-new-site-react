import React, { Component } from "react";
import "../../components/style.scss";
import "./style.scss";

export default class Button extends Component {
 index = this.props.currentScreen;
  render() {
    return (
      <div className="button-bottom" style={{position:"fixed",bottom:"0",right:"0"}}>
           <div className="next">
                <button className="button" onClick={this.props.setScreen(index)}>Next</button>
            </div> 
            <div className="previous">
                <button className="button">Previous</button>
            </div>  
      </div>      
    );
  }
}
