import React from "react";
import Services from "./services";

export default class Provide extends React.Component {
  render() {
    return (
      <div
        className="page leftSpace"
        id="what-we-do"
        style={{
          display: "block",
          height: window.innerWidth > 768 ? window.innerHeight : "auto",
          position: "static"
        }}
      >
        <div className="container">
          <div
            className="row page2-row"
            style={{ transform: "translate(-35px,30px)" }}
          >
            <div
              className="col-2 header sticky"
              style={{ alignItems: "center" }}
            >
              <a
                href="Homepage"
                className="anchor"
                style={{ paddingTop: "10px" }}
              >
                <img
                  src="/Front-images/logo.png"
                  alt="Armentum Logo"
                  className="logo"
                  style={{ width: 383, height: 62 }}
                />
              </a>
            </div>
          </div>
          <div className="row" style={{ flex: 2 }}>
            <div
              className="col centeralign"
              style={{ display: "flex", alignItems: "center" }}
            >
              <div>
                <h1 className="heading-1">
                  <span style={{ color: "#d94645" }}>W</span>e Partner
                </h1>
                <h3 className="heading-2">
                  with clients to get to product market fit and accelerate
                  growth
                </h3>

                {/* <a
                  className="teambtn"
                  onClick={() => {
                    this.props.setScreenNext(5);
                  }}
                > */}
                <a
                  className="button"
                  href="/pdfs/Armentum_Holdfolio_Case_Study.pdf"
                  style={{ textDecoration: "none", color: "#04dab9" }}
                  download
                >
                  Case Study: Fintech Client Raises $6.6mm on its Platform
                </a>
                {/* </a> */}
              </div>
            </div>
            <div
              className="col rightSpace"
              style={{ display: "flex", alignItems: "center" }}
            >
              <img
                src="/Front-images/back-circle-1.png"
                className="img-style"
                alt="circle"
              />
              <Services />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
