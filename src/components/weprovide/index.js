import React, { Component } from "react";

import Provide from "./provide";

import Layout2 from "../../views/Layout2";
import Layout3 from "../../views/Layout3";
import Layout4 from "../../views/Layout4";
import Layout5 from "../../views/Layout5";
import Layout6 from "../../views/Layout6";
import Layout7 from "../../views/Layout7";
import Layout8 from "../../views/Layout8";

import "../../components/style.scss";
import "./style.scss";

export default class WeProvide extends Component {
	render() {
		return <div className="parallax">
				<div className="parallax__layer parallax__layer--back">
					<Provide setScreenNext={this.props.setScreenNext} />
					<div className="child">
						<Layout2 setScreenNext={this.props.setScreenNext} />
						<Layout3 setScreenNext={this.props.setScreenNext} />
						<Layout4 setScreenNext={this.props.setScreenNext} />
						<Layout5 setScreenNext={this.props.setScreenNext} />
						<Layout6 setScreenNext={this.props.setScreenNext} />
						<Layout7 setScreenNext={this.props.setScreenNext} />
						<Layout8 setScreenNext={this.props.setScreenNext} />
					</div>
			 	</div>
			</div>;
	}
}
