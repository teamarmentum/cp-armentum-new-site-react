import React, { Component } from "react";
import $ from "jquery";
export default class Services extends Component {

	componentDidMount() {

	}

	onPress=(e)=>{
		e.preventDefault();
		var t = { scrollTop: $(e.currentTarget.hash).offset().top + 50 };
		$("html, body").animate(t, t.scrollTop/1.50);
		console.log($(e.currentTarget.hash).offset().top);
	}

	render() {
		return <div className="services" style={this.props.style}>
				<a href="#product-strategy" onClick={this.onPress}>
					<div className="service part1">
						<span className="service__image">
							<img src="/Front-images/page-planning.png" alt="Planning" />
						</span>
						<span className="service__title">Strategy</span>
					</div>
				</a>
				<a href="#revenue-engine-strategy" onClick={this.onPress}>
					<div className="service part2">
						<span className="service__image">
							<img src="/Front-images/page-coding.png" alt="Planning" />
						</span>
						<span className="service__title">Engineering</span>
					</div>
				</a>
				<a href="#staff-augmentation" onClick={this.onPress}>
					<div className="service part3">
						<span className="service__image ">
							<img src="/Front-images/page-class.png" alt="Planning" className="staffImg" />
						</span>
						<span className="service__title staffAlign">
							Staff Augmentation
						</span>
					</div>
				</a>
				<a href="#blockchain" onClick={this.onPress}>
					<div className="service part4">
						<span className="service__image">
							<img src="/Front-images/page-planning.png" alt="Planning" />
						</span>
						<span className="service__title">Blockchain</span>
					</div>
				</a>
			</div>;
	}
}
