import React, { Component } from "react";
import "./../../../index.css";

class Header extends Component {
  render() {
    return (
      <div className="logo">
        <img
          src="../../Front-images/logo-icon.png"
          className="logo-img"
          alt="image not displayed"
        />
        <img
          src="../../Front-images/logo-content.png"
          className="logo-content"
          alt="image not displayed"
        />
        <img
          src="../../Front-images/nav-icon.png"
          className="nav-img"
          alt="image not displayed"
        />
      </div>
    );
  }
}

export default Header;
