import React, { Component } from "react";
import "./../../../index.css";

class MainSection extends Component {
  render() {
    return (
      <div className="main-section row">
        <div className="main-section-back col-md-11 col-sm-11">
          <div className="row">
            <div className="main-section-1 col-md-6 col-sm-6">
              <div className="line-1">We build digital</div>
              <div className="line-2">experiences that</div>
              <div className="line-3">
                generate <span className="revenue-color">revenue</span>
              </div>
              <button className="btn-left-to-right">
                GAIN ACCESS TO OUR INDUSTRY MENTORS
              </button>
            </div>
            <div className="main-section-2 col-md-6 col-sm-6">
              <div className="row">
                <img
                  src="Front-images/back-top.png"
                  className="main-section-2-img6"
                  alt="image not displayed"
                />
                <img
                  src="Front-images/Laptop5.png"
                  className="main-section-2-img1"
                  alt="image not displayed"
                />
                <img
                  src="Front-images/Laptop4.png"
                  className="main-section-2-img2"
                  alt="image not displayed"
                />
                <img
                  src="Front-images/Laptop3.png"
                  className="main-section-2-img3"
                  alt="image not displayed"
                />
                <img
                  src="Front-images/Laptop2.png"
                  className="main-section-2-img4"
                  alt="image not displayed"
                />
                <img
                  src="Front-images/Laptop1.png"
                  className="main-section-2-img5"
                  alt="image not displayed"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="vertical-cursor col-md-1 col-sm-1">
          <ul className="dots">
            <li className="dot-1">
              <a href="#" className="adot-1" />
            </li>
            <li className="dot-2">
              <a href="#" className="adot-2" />
            </li>
            <li className="dot-3">
              <a href="#" className="adot-3" />
            </li>
            <li className="dot-4">
              <a href="#" className="adot-4" />
            </li>
            <li className="dot-5">
              <a href="#" className="adot-5" />
            </li>
            <li className="dot-6">
              <a href="#" className="adot-6" />
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default MainSection;
