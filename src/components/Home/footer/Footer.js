import React, { Component } from "react";
import "./../../../index.css";

class Footer extends Component {
  render() {
    return (
      <div className="back-footer">
        <img
          src="Front-images/information.png"
          className="info-icon"
          alt="image not displayed"
        />
        <div className="left-content">
          <span className="content-1">
            Our team has best the house of
            <span className="content-color">React Native</span>
            for cross-platform development.
          </span>
          <br />
          <span className="content-2">
            Is React Native right for you?
            <span className="content-color">Learn More Here</span>
          </span>
        </div>
      </div>
    );
  }
}

export default Footer;
