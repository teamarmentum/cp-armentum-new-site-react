import React, { Component } from "react";
import Navbar from "../../components/Page-2/navbar/Navbar";
import "../../components/style.scss";
import "./style.scss";

export default class AboutUs extends Component {
  render() {
    return (
      <div className="page aboutus">
        <div className="container">
          <div
            className="row top-row5"
            style={{ transform: "translate(-35px,15px)" }}
          >
            <div className="col-2" style={{ alignItems: "center" }}>
              <a href="Homepage" style={{ paddingTop: "10px" }}>
                <img
                  src="/Front-images/logo1.png"
                  className="logo"
                  alt="Armentum Logo"
                  style={{ width: 383, height: 62 }}
                />
              </a>
            </div>
          </div>
          <div className="row atarmentum" style={{ flex: 2 }}>
            <div
              className="col"
              style={{ display: "flex", alignItems: "center" }}
            >
              <div>
                <h1 className="heading-1">About us</h1>
                <h3 className="heading-3">
                  At Armentum, we do it differently. We started Armentum because
                  while there is a large selection of agencies to choose from,
                  60% of clients report that they are unhappy with their current
                  agency and are actively looking to switch.
                </h3>
                <a onClick={() => this.props.setScreenNext(5)}>
                  <button className="button" style={{ color: "blue" }}>
                    TALK TO OUR MENTORS
                  </button>
                </a>
              </div>
            </div>
            <div
              className="col"
              style={{ display: "flex", alignItems: "center" }}
            >
              <div>
                <p className="para">
                  There are literally thousands of agencies - why build another
                  one? Especially in a market that is this hyper competitive and
                  also commoditized. We started Armentum because while there is
                  a large selection of agencies to choose from, 60% of clients
                  report that they are unhappy with their current agency and are
                  collectively looking to switch. This is because the objectives
                  of the What rather than the Why. It typically lists out the
                  platform you are building an app out on (web or mobile) and
                  lists out the defined functionalities.{" "}
                </p>
                <p className="para">
                  At Armentum, we do it differently. Our Scope of Work is drive
                  by the Why - we focus on your Strategic Objectives and work
                  with you to leverage digital to help you build a better and
                  stronger business. This approach changes the client-agency
                  relationship. We are working together to achieve the same set
                  of objectives and because of this are significantly more
                  nimble and are able to product quality products and deliver
                  results faster and quicker.
                </p>
              </div>
              {/*<Navbar style={{right:"4%", top:"0%"}}/>*/}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
