import React from "react";
//import $ from "jquery";

class LoadingSpinner extends React.Component {
  render() {
    //   setTimeout(()=> {
    //     $("#overspin").fadeOut(1000, ()=> {
    //       // $("#overspin2").fadeIn(4000);
    //   });
    // });

    return (
      <div>
        <div
          id="overspin"
          style={{
            background: "rgba(0, 0, 0,1)",
            position: " absolute",
            top: 0,
            left: 0,
            // transform: "transition(2s,ease)",
            width: "100%",
            height: "100%"
          }}
        >
          <img
            src="/Front-images/loader.gif"
            className="fa-spin"
            alt=""
            style={{
              height: 150,
              width: 150,
              position: "absolute",
              left: "50%",
              top: "50%",
              marginLeft: "-75px",
              marginTop: "-75px"
            }}
          />
        </div>
        <div id="overspin2">{this.props.currentScreen}</div>
      </div>
    );
  }
}

export default LoadingSpinner;
