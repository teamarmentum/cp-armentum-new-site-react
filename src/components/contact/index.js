import React, { Component } from "react";
import "../../components/style.scss";
import "./style.scss";
import $ from "jquery";

export default class Contact extends Component {
                //  constructor() {
                //    this.state = { data: [] };
                //  }

                 apiCall=()=> {
                   let username = document.querySelector("#name").value;
                   let email = document.querySelector("#email").value;
                   let comments=document.querySelector("#comments").value; 
                   //check for username
                   if (username !== "" && username !== null && username.match(/^[A-Za-z ]+$/)) {
                     //check for email
                     if (/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/.test(email) && email !== "" && email !== null) {
                       // Agile CRM

                       let contact = {};
                       contact.email = email;
                       contact.first_name = username;
                       contact.comment=comments;
                       contact.tags = "Armentum";

                       window._agile.create_contact(
                         contact,
                         {
                           success: function(data) {
                             console.log("success", data);
                           },
                           error: function(data) {
                             document.getElementById("errorname1").innerHTML = "repeated email",data.error;
                           }
                         }
                       );
                     } else {
                       document.getElementById("errorname1").innerHTML = "*";
                     }
                   } else {
                     document.getElementById("errorname").innerHTML = "*";
                   }
                 }

                 componentDidMount() {
                   this.getFunction();
                 }

                 getFunction = () => {
                  //  this.apiCall().then(function(data){
                  //      console.log(data);
                  //      // set the state here
                  //      this.setState({ data: data });
                  //    }, function(error) {
                  //      console.log(error);
                  //    });
                 };
                // apiCall=()=>{
                //   var AgileCRMManager = require("agile_crm/agilecrm.js");
                //   var obj = new AgileCRMManager("curatedtalent", "tfmuo4s5h5938snhnsrdob0lif", "nikhilmanon37@gmail.com");
                //   var success = function (data) {
                //     console.log(data);
                //   };
                //   var error = function (data) {
                //     console.log(data);
                //   };
                  
                //   obj.contactAPI.getContactByEmail("nikhilmanon37@gmail.com", success, error);
                //   // $.post("http://localhost:8080/Armentum/armentum.php", {name:"papu",email:"abc@gmail.com",comments:"papu is successfull"}, function(data, status) {
                //   //   console.log(data); 
                //   //   debugger;                 
                //   // })
                // }
                 render() {
                   return <div className="page contact">
                       <div className="container">
                         <div className="row contact-row" style={{ transform: "translate(-35px,15px)" }}>
                           <div className="col-2" style={{ alignItems: "center" }}>
                             <a href="Homepage">
                               <img src="/Front-images/logo1.png" alt="Armentum Logo" className="logo" style={{ width: 383, height: 62, marginTop: "7px" }} />
                             </a>
                           </div>
                         </div>
                         <div className="row centerit" style={{ flex: 2 }}>
                           <div className="col remflex" style={{ display: "flex", alignItems: "center" }}>
                             <div>
                               <h1 className="heading-1">
                                 Contact
                               </h1>
                               <h3 className="heading-3">
                                 Armentum offices have the best coffee bar in town!<br />
                                 Come give us a visit!
                               </h3>
                               <div style={{ width: 300 }} className="col-div addr_class">
                                 <div className="centerdiv" style={{ width: 300 }}>
                                   <div id="usa_addr">
                                     <h2>USA</h2>
                                     <p>
                                       315 W 36th Street,<br />
                                       Suite 1010, New York, NY, 10018
                                     </p>
                                     <h5>
                                       +1(646)-582-9851
                                     </h5>
                                   </div>
                                   <hr style={{ color: "blue" }} id="horz_line" />
                                   <div id="india_addr">
                                     <h2>INDIA</h2>
                                     <p>
                                       4F02, Arya Hub Hope Farm Signal,<br />
                                       13/1B ITPL Road,<br />
                                       Whitefield Bengaluru, KA 560066
                                     </p>
                                     <h5>
                                       +91(804)-850-9806
                                     </h5>
                                   </div>
                                 </div>
                               </div>
                             </div>
                             <div className="col" id="form_contact" style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                               <div className="contact__form">
                                 <h3>
                                   Talk to an <span style={{ color: "black", fontWeight: "600" }}>
                                     ARMENTUM
                                   </span> business growth architect
                                 </h3>
                                 <form>
                                   <div className="input__group">
                                     <input type="text" onFocus={() => {
                                         $("#errorname").hide();
                                       }} name="name" id="name" placeholder="Enter your Name" style={{ position: "relative" }} />
                                     <span id="errorname" style={{ color: "red", fontSize: "20px", paddingLeft: "3px", position: "absolute", right: "20px", top: "17px" }}>
                                       {"*"}
                                     </span>
                                   </div>
                                   <div className="input__group">
                                     <input type="email" onFocus={() => {
                                         $("#errorname1").hide();
                                       }} name="email" id="email" placeholder="Enter your Email" style={{ position: "relative" }} />
                                     <span id="errorname1" style={{ color: "red", fontSize: "20px", paddingLeft: ".3px", position: "absolute", right: "20px", top: "17px" }}>
                                       {"*"}
                                     </span>
                                   </div>
                                   <div className="input__group">
                                     <textarea placeholder="How can we help?" id="comments" style={{ position: "relative" }} />
                                     <span id="errorname" style={{ color: "red", fontSize: "30px" }}>
                                       {" "}
                                     </span>
                                   </div>
                                 </form>
                                 <button className="button" onClick={this.apiCall} style={{ color: "blue", transform: "translateX(10px)" }}>
                                   GENERATE MORE REVENUE
                                 </button>
                               </div>
                               {/*<Navbar style={{right:"5%", top:"0%"}}/>*/}
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>;
                 }
               }
