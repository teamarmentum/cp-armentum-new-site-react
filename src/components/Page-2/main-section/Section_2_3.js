import React from "react";
import "../../../index.css";
import Right_section_5 from "../right-section-5/Right_section_5";
import $ from "jquery";
import { testimonials } from "../../../content/testimonial";

class Section_2_3 extends React.Component {
  handleClick(e) {
    e.preventDefault();
    $(".read_more_para").toggle(1000);
    if ($("#readMore").text() === "Read More") {
      console.log("read-more");
      var changeText = "Read Less";
      $("#readMore").text(changeText);
    } else {
      $("#readMore").text("Read More");
    }
  }

  render() {
    return (
      <div className="text-eng-3" id="divId3">
        <div className="container">
          <div className="row top-row-3">
            <div className="head-4-3 col-sm-8 col-md-8">
              <p className="top-heading-4-3">ENGINEERING</p>
              <div className="heading-4-3">
                <span className="product-color-4-3">Mobile Apps</span>
                <p className="top-bottom-4-3">Our Bet on React Native...</p>
              </div>
            </div>
          </div>
          <div className="row row-3">
            <div className="main-section-1-4-3 col-md-7 col-sm-7">
              <p className="line-1-4-3 justifyandcenter">
                React Native is truly a transformational evolution in
                cross-platform application development and Armentum recommends
                React Native for most of our clients. There are areas,
                especially around using third party IOT devices where React
                Native has integration issues- in those cases - we are able
                build that component in Swift for iOS and Java for Android and
                plug it into the code-base.
              </p>

              <p
                className="line-1-4-3 justifyandcenter read_more_para"
                style={{ display: " none" }}
              >
                This way, Single code written in React Native is used to run
                cross platform applications hence reducing the efforts and
                resources as well as save the cost.
              </p>
              <p>
                <a
                  href="#"
                  className="read-more-2-2"
                  id="readMore"
                  onClick={this.handleClick.bind(this)}
                >
                  Read More
                </a>
              </p>

              <img
                src="/Front-images/page2-4-img1.png"
                className="front-img-4-3"
              />
            </div>
            <div className="main-section-2-4-3 col-md-5 col-sm-5">
              <Right_section_5
                setScreenNext={this.props.setScreenNext}
                data={testimonials[0].data}
                by={testimonials[0].by}
                designation={testimonials[0].designation}
              />
            </div>
          </div>
          <br />
        </div>
      </div>
    );
  }
}

export default Section_2_3;
