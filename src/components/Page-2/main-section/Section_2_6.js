import React from "react";
import "../../../index.css";
import Right_section_5 from "../right-section-5/Right_section_5";
import { testimonials } from "../../../content/testimonial";

class Section_2_6 extends React.Component {
  render() {
    return (
      <div className="parent_div" id="divId6">
        <div className="container">
          <div className="row top-row-6" id="divId6">
            <div className="head-4-6">
              <div className="heading-4-6">
                <span className="product-color-4-6">Staff Augmentation</span>
                <p className="top-bottom-4-6">AWS For Engineers ... </p>
              </div>
            </div>
          </div>
          <div className="row med-row-6">
            <div className="main-section-1-4-6 col-md-7 col-sm-8">
              <div className="row row-img1-6">
                <div className="col-md-6 col-sm-6 color-col-6">
                  Armentum <br />has world class team of <br />React Native,
                  iOS, WordPress, and Node.Js developers.
                </div>
                <div className="col-md-6 col-sm-6 col-img-6">
                  <img
                    src="/Front-images/page2-7-hand.png"
                    className="front-img-4-6"
                    alt=""
                  />
                </div>
              </div>
              <div className="row row-img2-6">
                <p className="lower-para1-6">
                  We have spent months recruiting the best of the best and
                  developed intensive in-house training and mentorships programs
                  so that they engineers can continue to up their skill. For
                  team that handle in-house Product and/or Project Managers and
                  need to scale their engineering teams FAST, the Staff
                  Augmentation model works well.
                </p>
                <p className="lower-para2-6">
                  Our engineers plug into your team directly and that way you
                  are able to scale up and scale down the team as required.
                </p>
              </div>
            </div>
            <div className="main-section-2-4-6 col-md-5 col-sm-4">
              <Right_section_5
                setScreenNext={this.props.setScreenNext}
                style={{ color: "#aabcf2" }}
                style1={{ color: "white" }}
                style2={{ marginTop: "180px" }}
                data={testimonials[3].data}
                by={testimonials[3].by}
                designation={testimonials[3].designation}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Section_2_6;
