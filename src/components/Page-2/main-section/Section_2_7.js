import React from "react";
import "../../../index.css";
import Right_section_5 from "../right-section-5/Right_section_5";
import { testimonials } from "../../../content/testimonial";

class Section_2_7 extends React.Component {
  render() {
    return (
      <div className="text-eng-7" id="divId7">
        <div className="container">
          <div className="row top-row-7">
            <div className="col-sm-8 col-md-8 heading-4-7">
              <div className="heading-4-7">
                <span className="product-color-4-7">Blockchain</span>
                <p className="top-bottom-4-7">
                  Blockchain is the early leader ... They are actually making{" "}
                  <br />the entire ecosystem.
                </p>
              </div>
            </div>
          </div>

          <div className="row row-7">
            <div className="main-section-1-4-7 col-md-7 col-sm-8">
              <p className="line-1-4-7">
                It's time money caught up. Digital assets, like bitcoin and
                ether, allow users to transact directly without any third-party
                intermediary.
              </p>
              <div className="row row-img-7">
                <p className="top-medium-7 justifyandcenter">
                  “The blockchain is an incorruptible digital ledger of economic
                  transactions that can be programmed to record not just
                  financial transactions but virtually everything of value.” Don
                  & Alex Tapscott, authors Blockchain Revolution (2016).
                  Blockchain is durable,robust,transparent and incorruptible.
                  Enhanced security is achieved by storing data across the
                  network vis-a-vis centrally. Goldman Sachs believes that
                  blockchain technology holds great potential especially to
                  optimize clearing and settlements, and could represent global
                  savings of up to $6bn per year.
                </p>
              </div>
            </div>
            <div className="main-section-2-4-7 col-md-5 col-sm-4">
              <Right_section_5
                setScreenNext={this.props.setScreenNext}
                data={testimonials[4].data}
                by={testimonials[4].by}
                designation={testimonials[4].designation}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Section_2_7;
