import React from "react";
import "../../../index.css";
import $ from "jquery";

class Section_2_2 extends React.Component {
  handleClick(e) {
    e.preventDefault();
    $(".read_more_para").toggle(1000);
    if ($(".read-more-2-2").text() === "Read More") {
      $(".read-more-2-2").text("Read Less");
    } else {
      $(".read-more-2-2").text("Read More");
    }
    $(".read_more_para1").toggle(1000);
    if ($(".read-more-2-2").text() === "Read More") {
      $(".read-more-2-2").text("Read Less");
    } else {
      $(".read-more-2-2").text("Read More");
    }
  }

  handleClick1(e) {
    e.preventDefault();
    $(".read_more_para1").toggle(1000);
    if ($(".read-more-2-2").text() === "Read More") {
      $(".read-more-2-2").text("Read Less");
    } else {
      $(".read-more-2-2").text("Read More");
    }
  }

  render() {
    return (
      <div id="divId2">
        <div className="container">
          <div className="row headcenter">
            <div className="head-2-2">
              <p className="top-heading-2-2">STRATEGY</p>
              <div className="heading-2-2">
                <span className="product-color-2-2">
                  Revenue Engine
                  <br />Strategy
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="main-section-1-2-2 col-md-6 col-sm-6">
              <p className="line-1-2-2 justifyandcenter">
                Through rigorous A/B testing, we will work with you to find the
                Product Channel Fit for your company.
              </p>
              <p className="line-2-2-2 justifyandcenter">
                Unless you are a gas station on a busy intersection, believing
                in "Build it and they will come” is a foolhardy way to launch a
                new product or service. This holds especially true for digital
                products on both web and mobile where the competition for screen
                time is incredibly high.{" "}
              </p>

              <p
                className="line-1-2-2 justifyandcenter read_more_para"
                id="read_more_para1"
                style={{ display: " none" }}
              >
                Studies have proven that , a site visitor decides in the first
                15-20 seconds whether to stay on the site or leave. To capture
                the users attention in the impression is paramount and hence all
                marketing strategists suggest highlighting the relevant
                information on the site in the homepage. We work on the proven
                principles and hence enhancing the visitor base to the site and
                create a constant revenue stream.
              </p>
              <p>
                <a
                  href="#"
                  className="read-more-2-2"
                  id="readMore"
                  onClick={this.handleClick.bind(this)}
                >
                  Read More
                </a>
              </p>
            </div>
            <div className="main-section-2-2-2 col-md-6 col-sm-6">
              <img src="/Front-images/laptop4.png" className="right-col1-2-2" />
              <img
                src="/Front-images/white-stars.png"
                className="right-col2-2-2"
              />
              <img
                src="/Front-images/page2-3-col-img.png"
                className="right-col-2-2"
              />
            </div>
          </div>
          <br />
        </div>
      </div>
    );
  }
}

export default Section_2_2;
