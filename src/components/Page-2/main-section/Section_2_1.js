import React from "react";
import "../../../index.css";
import Right_section_5 from "../right-section-5/Right_section_5";

class Section_2_1 extends React.Component {
  render() {
    return (
      <div>
        <div id="divId1">
          <div className="container">
            <div className="head-2-1">
              <p className="top-heading-2-1">STRATEGY</p>
              <div className="heading-2-1">
                <span className="product-color-2-1">Product</span> Strategy
              </div>
            </div>
            <div className="row row-2-1">
              <div className="main-section-1-2-1 col-md-6 col-sm-6">
                <p className="line-1-2-1">
                  "According to Gartner, its costs $270,000 to design, build,
                  and launch an app. Even after investing a quarter of a million
                  dollars, only 0.01% of apps are actually concerned financially
                  successful.”
                </p>
                <p className="line-2-2-1">
                  By applying elements of design thinking and lean methodology,
                  we work with you to understand the market, the competitive
                  landscape, trends, and the key pain points of the various
                  stakeholders.
                </p>
                <img
                  src="/Front-images/img-visual-strategy-new.png"
                  className="product-strategy-2-1"
                />
              </div>
              <div className="main-section-2-2-1 col-md-6 col-sm-6">
                <div className="testimonial-2-1">
                  <p>
                    <div className="test-color-2-1">Testimonial</div>{" "}
                    <div className="point-2-1">
                      ..................................................................
                    </div>
                  </p>
                  <p className="test-content-2-1" placeholder="">
                    Armentum provides GREAT value. They provide reasonable
                    pricing, stick to their quotes, and never nickel and dime.
                    I've done a ton of development with them and aside from the
                    great value they provide, I've really enjoyed their design
                    team and management technology to help make providing input
                    and managing the process as easy as possible.
                  </p>
                  <p className="name-2-1">Jacob Blackett,</p>
                  <p className="name-2-1">Co-Founder & CEO, Holdfolio.com </p>
                </div>
                <div className="blog-2-1">
                  <a
                    onClick={() => {
                      this.props.setScreenNext(5);
                    }}
                  >
                    <button className="btn-suggestion-2-1">
                      <div className="phone-2-1">
                        <img
                          src="/Front-images/call.png"
                          className="phone-call-2-1"
                        />
                      </div>
                      <div className="suggest-2-1">
                        Get a 30 Minute Product<br />
                        Strategy Consultation
                      </div>
                    </button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Section_2_1;
