import React from "react";
import "../../../index.css";
import Right_section_5 from "../right-section-5/Right_section_5";
import $ from "jquery";
import { testimonials } from "../../../content/testimonial";

class Section_2_4 extends React.Component {
  handleClick(e) {
    e.preventDefault();
    $(".para1").toggle(1000);
    if ($(".read-more-4-1").text() === "Read More") {
      $(".read-more-4-1").text("Read More");
    } else {
      $(".read-more-4-1").text("Read More");
    }
  }

  render() {
    return (
      <div className="text-eng-4" id="divId4">
        <div className="container">
          <div className="row top-row-4">
            <div className="head-4-4 row headcenter">
              <div className="col-sm-8 col-md-8 text spacetext">
                <p className="top-heading-4-3">ENGINEERING</p>
                <div className="heading-4-4">
                  <span className="product-color-4-4">Web Apps</span>
                  <p className="top-bottom-4-4">
                    Our Bet on WordPress/PHP Powered Web Apps
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="row med-row-4">
            <div className="main-section-1-4-4 col-md-7 col-sm-8">
              <div className="row row-img1-4">
                <div className="col-md-6 col-sm-6 color-col-4">
                  <p className="para-1-1-4 widthandmax">
                    Isn’t WordPress a blogging platform? Aren’t you supposed to
                    use Node.Js or something like Python to build a web app? How
                    can you build powerful Web Apps using WordPress
                  </p>
                  <p className="para-1-2-4 widthandmax">
                    Let’s answer these question one by one. WordPress surely
                    began as a blogging platform and because of hundreds of
                    thousands of developers continuously writing plugins and
                    releasing open source code, WordPress has grown from a
                    blogging platform to a platform through which you are able
                    to build some solid Web-Apps. Billion dollar companies such
                    as Groupon began on WordPress. As WordPress is a CMS powered
                    by PHP, we are able to write code, convert it into a plugin,
                    and integrate it within our WordPress App.
                  </p>
                  <a
                    className="read-more-4-1"
                    id="readMore"
                    style={{ pointer: "cursor" }}
                  >
                    Read More
                  </a>
                </div>
                <div className="col-md-6 col-sm-6 col-img-4 para1">
                  <p className="para-2-1-4 widthandmax">
                    Building out the web-app using WordPress mostly benefits
                    non-technical teams because there is a simple interface for
                    their dashboard. It also helps to launch quicker and
                    cheaper. By getting to market more efficiently, we are able
                    to test and iterate significantly quicker and thereby
                    getting to Product Market Fit Faster.
                  </p>
                  <p className="para2-2-4 widthandmax">
                    There are off-course instances and apps where we do need to
                    Python, Node.Js, Scala etc are better. For instance, with
                    fintech apps, Python is typically the preferred language of
                    choice because it require lots of data processing. In such
                    cases, we recommend writing the specific
                    feature/functionality related to data processing on Python
                    and integrate that within our code base using APIs.
                  </p>
                </div>
              </div>
            </div>
            <div className="main-section-2-4-4 col-md-5 col-sm-4">
              <Right_section_5
                setScreenNext={this.props.setScreenNext}
                data={testimonials[1].data}
                by={testimonials[1].by}
                designation={testimonials[1].designation}
              />
            </div>
          </div>
        </div>
        <div className="row bottom-row-4" style={{ display: "none" }}>
          <div
            id="carousel-id"
            className="carousel slide"
            data-ride="carousel"
            style={{
              background: "#fff",
              paddingTop: "35px",
              paddingBottom: "35px"
            }}
          >
            <ol className="carousel-indicators">
              <li
                data-target="#carousel-id"
                data-slide-to="0"
                className="active"
              />
              <li data-target="#carousel-id" data-slide-to="1" className="" />
              <li data-target="#carousel-id" data-slide-to="2" className="" />
            </ol>
            {/*<div className="carousel-inner">
										<div className="item active">

											<div className="container">
												<div className="carousel-caption row bottom-row-1-4">
													<div className="leftcolumn1-4">
														<div className="fakeimg-4-4">
															<img src="/Front-images/	bottom-row-img1.png" className="fake-img-4-4"/>
														</div>
													</div>
													<div className="rightcolumn1-4">
																		<p className="blog1-4-4">Disciplined Product Strategy</p>
																		<p className=" blog2-4-4">When a company gets to certain stage, there will be many ...
																		</p>
																		<p><a className="read-more-4-4" href="#">Read More</a></p>
													</div>
												</div>

											</div>
										</div>
									</div>
										<div className="item">\
										 <div className="row bottom-row-1-4">
													<div className="leftcolumn">
															<div className="fakeimg-2-4">
																	<img src="/Front-images/	bottom-row-img2.png" className="fake-img-4-4"/>
														</div>
													</div>
													<div className="rightcolumn">
															<p className="blog1-4-4">Disciplined Product Strategy</p>
															<p className=" blog2-4-4">When a company gets to certain stage, there will be many ...
															</p>
															<p><a className="read-more-4-4" href="#">Read More</a></p>
															</div>
												</div>
											</div>
										</div>
									<a className="left carousel-control" href="#carousel-id" data-slide="prev"><span className="glyphicon glyphicon-chevron-left"></span></a>
									<a className="right carousel-control" href="#carousel-id" data-slide="next"><span className="glyphicon glyphicon-chevron-right"></span></a>*/}

            <div className="carousel-inner" role="listbox">
              <p className="para-top">Case Studies</p>
              <div className="item active left">
                <div className="container custom">
                  <div className="row upper-row">
                    <div className="col-sm-6 col-md-6">
                      <div className="row bottom-row-1-4">
                        <div className="leftcolumn-4">
                          <div className="fakeimg-2-4">
                            <img
                              src="/Front-images/	bottom-row-img2.png"
                              className="fake-img-pic"
                            />
                          </div>
                        </div>
                        <div className="rightcolumn-4">
                          <p className="blog1-cont">
                            Disciplined Product Strategy
                          </p>
                          <p className=" blog2-cont">
                            When a company gets to certain stage, there will be
                            many ...
                          </p>
                          <p>
                            <a className="read-more-4-4" href="#">
                              Read More
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-6">
                      <div className="row bottom-row-1-4">
                        <div className="leftcolumn-4">
                          <div className="fakeimg-2-4">
                            <img
                              src="/Front-images/	bottom-row-img2.png"
                              className="fake-img-pic"
                            />
                          </div>
                        </div>
                        <div className="rightcolumn-4">
                          <p className="blog1-cont">
                            Disciplined Product Strategy
                          </p>
                          <p className=" blog2-cont">
                            When a company gets to certain stage, there will be
                            many ...
                          </p>
                          <p>
                            <a className="read-more-4-4" href="#">
                              Read More
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="item next left">
                <div className="container custom">
                  <div className="row upper-row">
                    <div className="col-sm-6 col-md-6">
                      <div className="row bottom-row-1-4">
                        <div className="leftcolumn-4">
                          <div className="fakeimg-2-4">
                            <img
                              src="/Front-images/	bottom-row-img2.png"
                              className="fake-img-pic"
                            />
                          </div>
                        </div>
                        <div className="rightcolumn-4">
                          <p className="blog1-cont">
                            Disciplined Product Strategy
                          </p>
                          <p className=" blog2-cont">
                            When a company gets to certain stage, there will be
                            many ...
                          </p>
                          <p>
                            <a className="read-more-4-4" href="#">
                              Read More
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-6">
                      <div className="row bottom-row-1-4">
                        <div className="leftcolumn-4">
                          <div className="fakeimg-2-4">
                            <img
                              src="/Front-images/	bottom-row-img2.png"
                              className="fake-img-pic"
                            />
                          </div>
                        </div>
                        <div className="rightcolumn-4">
                          <p className="blog1-cont">
                            Disciplined Product Strategy
                          </p>
                          <p className=" blog2-cont">
                            When a company gets to certain stage, there will be
                            many ...
                          </p>
                          <p>
                            <a className="read-more-4-4" href="#">
                              Read More
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="item">
                <div className="container custom">
                  <div className="row upper-row">
                    <div className="col-sm-6 col-md-6">
                      <div className="row bottom-row-1-4">
                        <div className="leftcolumn-4">
                          <div className="fakeimg-2-4">
                            <img
                              src="/Front-images/	bottom-row-img2.png"
                              className="fake-img-pic"
                            />
                          </div>
                        </div>
                        <div className="rightcolumn-4">
                          <p className="blog1-cont">
                            Disciplined Product Strategy
                          </p>
                          <p className=" blog2-cont">
                            When a company gets to certain stage, there will be
                            many ...
                          </p>
                          <p>
                            <a className="read-more-4-4" href="#">
                              Read More
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-6">
                      <div className="row bottom-row-1-4">
                        <div className="leftcolumn-4">
                          <div className="fakeimg-2-4">
                            <img
                              src="/Front-images/	bottom-row-img2.png"
                              className="fake-img-pic"
                            />
                          </div>
                        </div>
                        <div className="rightcolumn-4">
                          <p className="blog1-cont">
                            Disciplined Product Strategy
                          </p>
                          <p className=" blog2-cont">
                            When a company gets to certain stage, there will be
                            many ...
                          </p>
                          <p>
                            <a className="read-more-4-4" href="#">
                              Read More
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <a
                className="left carousel-control"
                href="#myCarousel"
                role="button"
                data-slide="prev"
              >
                <span
                  className="glyphicon glyphicon-menu-left"
                  aria-hidden="true"
                />
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="right carousel-control"
                href="#myCarousel"
                role="button"
                data-slide="next"
              >
                <span
                  className="glyphicon glyphicon-menu-right"
                  aria-hidden="true"
                />
                <span className="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Section_2_4;
