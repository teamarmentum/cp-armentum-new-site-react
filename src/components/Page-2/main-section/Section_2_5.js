import React from "react";
import "../../../index.css";
import Right_section_5 from "../right-section-5/Right_section_5";
import { testimonials } from "../../../content/testimonial";

class Section_2_5 extends React.Component {
  render() {
    return (
      <div className="text-eng-5" id="divId5">
        <div className="container">
          <div className="row top-row-5 centerit">
            <div className="col-sm-8 col-md-8 text">
              <p className="top-heading-4-5">ENGINEERING</p>
              <div className="heading-4-5">
                <span className="product-color-4-5">Marketing Website</span>
                <p className="top-bottom-4-5">
                  Your Site Should Be Your Best Salesperson and Marketer ...
                </p>
              </div>
            </div>
          </div>
          <br />
          <br />

          <div className="row row-5">
            <div className="main-section-1-4-5 col-md-7 col-sm-8">
              <p className="line-1-4-5 widthandmax1">
                For majority of brick and mortar businesses, their website is
                just a marker to add the most basic legitimacy that they exist -
                that should not be the case. Your website should work to
                generate your leads, warm them up, and incentivize them to reach
                out to you via phone/email or come see you at your store.
              </p>
              <div className="row row-img-5">
                <div className="col-md-6 col-sm-6 col-img-5">
                  <span className="span-color widthandmax1">At Armentum</span>,
                  we work with our clients to brand their business as a
                  world-className one and to leverage sales and marketing
                  automation tools to generate leads and warm up prospects. Our
                  engagements on Business Informational Sites typically begin
                  with a one-time site set-up costs and we work with our clients
                  on a monthly retainer to help them brand and generate leads.
                </div>
                <div className="image col-md-6 col-sm-6">
                  <img
                    src="/Front-images/img-bar-chart.png"
                    className="front-img-4-5"
                  />
                </div>
              </div>
            </div>
            <div className="main-section-2-4-5 col-md-5 col-sm-4">
              <Right_section_5
                setScreenNext={this.props.setScreenNext}
                data={testimonials[2].data}
                by={testimonials[2].by}
                designation={testimonials[2].designation}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Section_2_5;
