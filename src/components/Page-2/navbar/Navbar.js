import React from "react";
import "../../../index.css";
import $ from "jquery";
class Navbar extends React.Component {
	openNav = () => {
			document.getElementById("myNav").style.display = "block";
		setTimeout(() => {
			var a = document.getElementById("myNav");
			// a.style.visibility = "visible";
			a.style.zIndex = 2;
			a.style.width = "100%";
			a.style.height = "100%";
			a.style.top = "0";
			a.style.opacity = "1";
			a.style.right = "0";
		}, 100);
	};

	closeNav = () => {
		var b = document.getElementById("myNav");
		b.style.zIndex = -999999;
		b.style.width = "80%";
		b.style.opacity="0";
		b.style.height = "80%";
		b.style.top = "10%";
		b.style.right = "10%";
		setTimeout(() => {
			document.getElementById("myNav").style.display="none";
		}, 100);
	};



	// handleClick = () => {
	//   var $this = $(this);
	//   this.props.setScreenNext(0);
	//   setTimeout(function(that) {
	//     //$(".a-link1").collapse("hide");
	//   }, 350);
	// };

	render() {
		return (
			<div className="nv-bar">
				<div className="navlink">
					<span
						style={{ fontSize: "30px", cursor: "pointer"}}
						onClick={this.openNav}
					>
						<img
							src="Front-images/nav-icon.png"
							className="nav-img-4"
							alt="nav-icon"
						/>
					</span>

					<div id="myNav" className="overlay">
						<a
							href={"javascript:void(0)"}
							className="closebtn"
							onClick={this.closeNav}
						>
							&times;{/*<img src="Front-images/nav-icon.png" className="nav-img-4" alt="nav-icon" /> */}
						</a>
						<div className="overlay-content">
							<a
								className="a-link1"
								onClick={() => {
									this.props.setScreenNext(0);
									this.closeNav();
								}}
								style={{ cursor: "pointer" }}
							>
								<span className="nav-img">
									<img
										src="/Front-images/icon-home.png"
										alt="About-us"
										className="nav-pics"
									/>
								</span>
								<span className="span-content-1">Home</span>
							</a>
							<hr className="hr-line1" />
							<a
								className="a-link2"
								onClick={() =>{
									this.props.setScreenNext(1);
									this.closeNav();
								}}
								style={{ cursor: "pointer" }}
							>
								<span className="nav-img">
									<img
										src="/Front-images/icon-strategy.png"
										alt="About-us"
										className="nav-pics"
									/>
								</span>
								<span className="span-content-2">What we do</span>
							</a>
							<hr className="hr-line2" />
							<a
								className="a-link3"
								onClick={() => {
									this.props.setScreenNext(2);
									this.closeNav();
								}}
								style={{ cursor: "pointer" }}
							>
								<span className="nav-img">
									<img
										src="/Front-images/icon-planning-strategy.png"
										alt="About-us"
										className="nav-pics"
									/>
								</span>
								<span className="span-content-3">How we work</span>
							</a>
							<hr className="hr-line3" />
							<a
								className="a-link4"
								onClick={() => {
									this.props.setScreenNext(3);
									this.closeNav();
								}}
								style={{ cursor: "pointer" }}
							>
								<span className="nav-img">
									<img
										src="/Front-images/icon-people.png"
										alt="About-us"
										className="nav-pics"
									/>
								</span>
								<span className="span-content-4">About us</span>
							</a>
							<hr className="hr-line4" />
							<a className="a-link5" onClick={() => {
									this.props.setScreenNext(4);
									this.closeNav();
								}}
								style={{ cursor: "pointer" }}
							>
								<span className="nav-img">
									<img
										src="/Front-images/icon-technology.png"
										alt="About-us"
										className="nav-pics"
									/>
								</span>
								<span className="span-content-5">Team</span>
							</a>
							<hr className="hr-line5" />
							<a className="a-link6" onClick={() => {
									this.props.setScreenNext(5);
									this.closeNav();
								}}
								style={{ cursor: "pointer" }}
							>
								<span className="nav-img">
									<img
										src="/Front-images/icon-envelop.png"
										alt="About-us"
										className="nav-pics"
									/>
								</span>
								<span className="span-content-6">Contact us</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Navbar;
