import React from "react";
import "../../../index.css";

class Right_section_5 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      by: "",
      designation: ""
    };
  }
  componentDidMount() {
    let data = this.props.data || "";
    let by = this.props.by || "";
    let designation = this.props.designation || "";
    this.setState({
      data: data,
      by: by,
      designation: designation
    });
  }
  render() {
    return (
      <div
        className="parent-div-5 remove_padding div-spacing"
        style={this.props.style2}
      >
        <div className="bracket-1">
          {/*}		style={{backgroundColor:this.props.colr}} */}

          <p className="test-content-4" style={this.props.style1}>
            " {this.state.data} "
          </p>
          <p className="name-4 des-content">{this.state.by},</p>
          <p className="name-4 designation sign">{this.state.designation}</p>
        </div>

        <div className="btn-suggest">
          <a
            onClick={() => {
              this.props.setScreenNext(5);
            }}
          >
            <button className="btn-suggestion-4">
              <div className="phone-4">
                <img src="/Front-images/call.png" className="phone-call-4" />
              </div>
              <div className="suggest-4" style={this.props.style1}>
                Get a 30 Minute Product<br />
                Strategy Consultation
              </div>
            </button>
          </a>
        </div>
      </div>
    );
  }
}

export default Right_section_5;
