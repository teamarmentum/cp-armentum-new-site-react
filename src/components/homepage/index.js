import React, { Component } from "react";
import "../../components/style.scss";
import "./style.scss";

export default class Homepage extends Component {
  render() {
    return (
      <div className="page" id="homepage">
        <div className="container centerit">
          <div
            className="row top-row"
            style={{ transform: "translate(-35px,30px)" }}
          >
            <div className="col-2" style={{ alignItems: "center" }}>
              <a href="Homepage" style={{ paddingTop: "4px" }}>
                <img
                  src="/Front-images/logo.png"
                  alt="Armentum Logo"
                  className="logo"
                  style={{ width: 383, height: 62 }}
                />
              </a>
            </div>
            {/* <div>
              <img
                src="/Front-images/nav-icon.png"
                alt="Nav Icon"
                style={{ width: 50 }}
              />
            </div> */}
          </div>
          <div className="row row-2" style={{ flex: 2 }}>
            <div
              className="col column-1"
              style={{ display: "flex", alignItems: "center" }}
            >
              <div>
                <h1 className="head1">We build digital</h1>
                <h1 className="head2">
                  experiences that generate{" "}
                  <span className="span-color">revenue</span>
                </h1>
                <a
                  className="teambtn"
                  onClick={() => {
                    this.props.setScreenNext(1);
                  }}
                >
                  <button
                    className="button"
                    style={{ color: "", fontSize: "20px" }}
                  >
                    0.01% Product Succeed. Will Yours?
                  </button>
                </a>
              </div>
            </div>
            <div
              className="col column-2"
              style={{ display: "flex", alignItems: "center" }}
            >
              <img
                src="/Front-images/home-laptop.png"
                alt="Laptop"
                className="laptop__img"
              />
            </div>
          </div>
          <div className="row bottomSpace" style={{ paddingBottom: 5 }}>
            <div style={{ paddingRight: 15 }}>
              <img
                src="/Front-images/information.png"
                alt="Info"
                style={{ width: 40 }}
              />
            </div>
            <div className="foot">
              <p style={{ color: "#5882fb", fontSize: "13px" }}>
                Masters of Photography recouped its MVP Development investment
                in less than a week!{" "}
              </p>
              <p style={{ color: "#5882fb", fontSize: "13px" }}>
                To learn more{" "}
                <span style={{ color: "#b1cb4e", fontWeight: "600" }}>
                  <a
                    href="/pdfs/Armentum_MOP_Case_Study.pdf"
                    style={{
                      color: "#b1cb4e",
                      fontWeight: "600",
                      cursor: "pointer",
                      textDecoration: "none"
                    }}
                    download
                  >
                    Download The Case Study!
                  </a>
                </span>{" "}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
