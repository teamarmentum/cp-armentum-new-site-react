import React, { Component } from "react";
//import Navbar from "../../components/Page-2/navbar/Navbar";
import "./style.scss";

export default class OurTeam extends Component {
  render() {
    return (
      <div className="team" id="ourteam">
        <div className="page our-team">
          <div className="container">
            <div className="row" style={{ transform: "translate(-35px,15px)" }}>
              <div className="col-2" style={{ alignItems: "center" }}>
                <a href="Homepage">
                  <img
                    src="/Front-images/logo1.png"
                    className="logo"
                    alt="Armentum Logo"
                    style={{ width: 383, height: 62 }}
                  />
                </a>
              </div>
            </div>
            <div className="row flexremove" style={{ flex: 2 }}>
              <div className="col teamsection">
                <div>
                  <h1 className="heading-1">
                    Team <span className="strength">is greatest strength.</span>
                  </h1>
                  <h3
                    style={{
                      fontFamily: "Open Sans",
                      fontWeight: 400,
                      fontSize: 24,
                      paddingRight: 60,
                      lineHeight: "30px",
                      paddingBottom: 0,
                      paddingTop: 0,
                      color: "#333"
                    }}
                    className="heading-3"
                  >
                    In Armentum, we firmly that our team is our family and
                    working together enhances the bond that we share. Majority
                    of our awake time is spent at work and the bonds formed last
                    for a lifetime as well. Work-life balance is very important
                    to us as well as working with a smile.
                  </h3>

                  <a
                    style={{ textDecoration: "none", color: "blue" }}
                    onClick={() => this.props.setScreenNext(5)}
                  >
                    <button className="button">JOIN OUR TEAM</button>
                  </a>
                </div>
              </div>
              <div
                className="col"
                style={{ display: "flex", alignItems: "center" }}
              >
                <div>
                  <img
                    src="/Front-images/img-team.png"
                    className="team-pic"
                    alt="team-image"
                  />
                </div>
                {/*<Navbar style={{right:"3%", top:"0%"}}/>*/}
              </div>
            </div>
          </div>
        </div>
        {/* <div className="page meet-team" id="meet-team">
          <div class="container aligncenter">
            <h1
              className="head-color"
              style={{ color: "blue", fontWeight: "800",paddingBottom:"35px",marginLeft:"-15px" }}
            >
              Meet the team
            </h1>
            <div className="row">
              <div className="col" style={{ paddingBottom: "30px" }}>
                <div className="team__member">
                  <img
                    src="https://www.placehold.it/150x160"
                    alt="Ameet Mehta"
                    style={{
                      width: "255px",
                      height: "250px"
                    }}
                  />
                  <div>
                    <h2 class="ameet"style={{ fontSize: "15px", fontWeight:"700", paddingLeft:"70px", marginTop:"10px" }}>AMEET MEHTA</h2>
                    <h4 class="ameet"style={{ color: "#8f8f8f", fontWeight:"400",fontSize:"13px",paddingLeft:"115px",marginTop:"-6px" }}>CEO</h4>
                  </div>
                </div>
              </div>
              <div className="col">
                <div className="team__member">
                  <img
                    src="https://www.placehold.it/150x160"
                    alt="Ameet Mehta"
                    style={{
                      width: "255px",
                      height: "250px"
                    }}
                  />
                  <div>
                    <h2 class="dinesh" style={{ fontSize: "15px", fontWeight: "700", paddingLeft: "85px", marginTop: "10px" }}>DINESH JAIN</h2>
                    <h4 class="dinesh" style={{ color: "#8f8f8f", fontWeight: "400", fontSize: "13px",paddingLeft: "45px", marginTop: "-6px" }}>
                      VP Of Product &amp; Operations
                    </h4>
                  </div>
                </div>
              </div>
              <div className="col" style={{ paddingBottom: "30px" }}>
                <div className="team__member">
                  <img
                    src="https://www.placehold.it/150x160"
                    alt="Ameet Mehta"
                    style={{
                      width: "255px",
                      height: "250px"
                    }}
                  />
                  <div>
                    <h2 class="padmaja" style={{ fontSize: "15px", fontWeight: "700", paddingLeft: "90px", marginTop: "10px" }}>PADMAJA</h2>
                    <h4 class="padmaja" style={{ color: "#8f8f8f", fontWeight: "400", fontSize: "13px", paddingLeft: "75px", marginTop: "-6px" }}>Project Manager</h4>
                  </div>
                </div>
              </div>
              <div className="col" style={{ paddingBottom: "30px" }}>
                <div className="team__member">
                  <img
                    src="https://www.placehold.it/150x160"
                    alt="Ameet Mehta"
                    style={{
                      width: "255px",
                      height: "250px"
                    }}
                  />
                  <div>
                    <h2 class="kuntal" style={{ fontSize: "15px", fontWeight: "700", paddingLeft: "50px", marginTop: "10px" }}>KUNTAL MARFATIA</h2>
                    <h4 class="kuntal" style={{ color: "#8f8f8f", fontWeight: "400", fontSize: "13px",paddingLeft: "70px", marginTop: "-6px" }}>Project Manager</h4>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col" style={{ paddingBottom: "30px" }}>
                <div className="team__member">
                  <img
                    src="https://www.placehold.it/150x160"
                    alt="Ameet Mehta"
                    style={{
                      width: "255px",
                      height: "250px"
                    }}
                  />
                  <div>
                    <h2 class="ameet" style={{ fontSize: "15px", fontWeight: "700", paddingLeft: "70px", marginTop: "10px" }}>AMEET MEHTA</h2>
                    <h4 class="ameet" style={{ color: "#8f8f8f", fontWeight: "400", fontSize: "13px", paddingLeft: "115px", marginTop: "-6px" }}>CEO</h4>
                  </div>
                </div>
              </div>
              <div className="col" style={{ paddingBottom: "30px" }}>
                <div className="team__member">
                  <img
                    src="https://www.placehold.it/150x160"
                    alt="Ameet Mehta"
                    style={{
                      width: "255px",
                      height: "250px"
                    }}
                  />
                  <div>
                    <h2 class="ameet"style={{ fontSize: "15px", fontWeight: "700", paddingLeft: "85px", marginTop: "10px" }}>DINESH JAIN</h2>
                    <h4 class="ameet"style={{ color: "#8f8f8f", fontWeight: "400", fontSize: "13px", paddingLeft: "45px", marginTop: "-6px" }}>
                      VP Of Product &amp; Operations
                    </h4>
                  </div>
                </div>
              </div>
              <div className="col" style={{paddingBottom: "30px"}}>
                <div className="team__member">
                  <img
                    src="https://www.placehold.it/150x160"
                    alt="Ameet Mehta"
                    style={{
                      width: "255px",
                      height: "250px"
                    }}
                  />
                  <div>
                    <h2 class="ameet"style={{ fontSize: "15px", fontWeight: "700", paddingLeft: "90px", marginTop: "10px" }}>PADMAJA</h2>
                    <h4 class="ameet"style={{ color: "#8f8f8f", fontWeight: "400", fontSize: "13px", paddingLeft: "75px", marginTop: "-6px" }}>Project Manager</h4>
                  </div>
                </div>
              </div>
              <div className="col" style={{paddingBottom:"30px"}}>
                <div className="team__member">
                  <img
                    src="https://www.placehold.it/150x160"
                    alt="Ameet Mehta"
                    style={{
                      width: "255px",
                      height: "250px"
                    }}
                  />
                  <div>
                    <h2 class="ameet" style={{ fontSize: "15px", fontWeight: "700", paddingLeft: "50px", marginTop: "10px" }}>KUNTAL MARFATIA</h2>
                    <h4 class="ameet" style={{ color: "#8f8f8f", fontWeight: "400", fontSize: "13px", paddingLeft: "70px", marginTop: "-6px" }}>Project Manager</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="join-team" id="jointeam">
          <h1>Join our team!</h1>
          <div>
            <button className="button"> VIEW CURRENT OPENGINGS </button>
            <button className="button"> LEARN ABOUT THE COMPANY </button>
                  </div>*/}
      </div>
    );
  }
}
