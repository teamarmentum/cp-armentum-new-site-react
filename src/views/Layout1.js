import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import '../index.css';
import WeProvide from "./../components/weprovide/index";
import Navbar from './../components/Page-2/navbar/Navbar';

class Layout1 extends Component{
  

  render(){
    return(
      
      	<div className="main-section-2 row">
      		
      		<div className="main-section-back-2 col-md-11 col-sm-11">
      			<div className="col-sm-12 col-md-12">
            <WeProvide/>
            
			    </div>
       		<div className="vertical-cursor col-md-1 col-sm-1">
       			<Navbar/>
        	</div>
        </div>
      </div>

    )
  }
}

export default Layout1;