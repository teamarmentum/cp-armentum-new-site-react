import React, { Component } from "react";
//import { BrowserRouter as Router, Route } from "react-router-dom";
import "../index.css";
//import WeProvide from "./../components/weprovide/index";
//import Navbar from './../components/Page-2/navbar/Navbar';
import Section_2_4 from "./../components/Page-2/main-section/Section_2_4";

class Layout5 extends Component {
  render() {
    return (
      <div className="main-section-2 row" id="web-apps">
        <div className="main-section-back-2 col-md-11 col-sm-11">
          <div className="col-sm-12 col-md-12">
            <Section_2_4 setScreenNext={this.props.setScreenNext} />
          </div>
        </div>
        <div className="vertical-cursor col-md-1 col-sm-1">
          {/*<Navbar setScreen={this.props.setScreen} currentState={this.props.currentState}/>*/}
        </div>
      </div>
    );
  }
}

export default Layout5;
