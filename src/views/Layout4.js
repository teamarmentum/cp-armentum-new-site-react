import React from "react";
//import { BrowserRouter as Router, Route } from "react-router-dom";
import "../index.css";
//import WeProvide from "./../components/weprovide/index";
//import Navbar from "./../components/Page-2/navbar/Navbar";
import Section_2_3 from "./../components/Page-2/main-section/Section_2_3";

class Layout4 extends React.Component {
  render() {
    return (
      <div className="main-section-2 row" id="mobile-apps">
        <div className="main-section-back-2 col-md-11 col-sm-11">
          <div className="col-sm-12 col-md-12">
            <Section_2_3 setScreenNext={this.props.setScreenNext} />
          </div>
        </div>
        <div className="vertical-cursor col-md-1 col-sm-1">
          {/*<Navbar setScreen={this.props.setScreen} currentState={this.props.currentState}/>*/}
        </div>
      </div>
    );
  }
}

export default Layout4;
