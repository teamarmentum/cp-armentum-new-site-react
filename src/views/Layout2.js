import React, { Component } from "react";
//import { BrowserRouter as Router, Route } from "react-router-dom";
import "../index.css";
//import WeProvide from "./../components/weprovide/index";
//import Navbar from './../components/Page-2/navbar/Navbar';
import Section_2_1 from "./../components/Page-2/main-section/Section_2_1";

class Layout2 extends Component {
  render() {
    return (
      <div className="main-section-2 row" id="product-strategy">
        <div className="main-section-back-2 col-md-11 col-sm-11">
          <div className="col-sm-12 col-md-12">
            <Section_2_1
              style={{ transition: "all 10s linear", display: "none" }}
              setScreenNext={this.props.setScreenNext}
            />
          </div>
        </div>
        <div className="vertical-cursor col-md-1 col-sm-1">
          {/* <Navbar /> */}
        </div>
      </div>
    );
  }
}

export default Layout2;
