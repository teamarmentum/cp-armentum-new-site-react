import React, { Component } from "react";
import Homepage from "./../components/homepage/index";
import AboutUs from "./../components/aboutus/index";
import Contact from "./../components/contact/index";
import WeProvide from "./../components/weprovide/index";
import OurTeam from "./../components/team/index";
import Howwework from "./../components/howwework/index";
import LoadingSpinner from "./../components/spinner/index";
import "../index.css";
import "../views/style.scss";
import Navbar_2 from "../components/Page-2/navbar/Navbar_2";
import $ from "jquery";
import Navbar from "../components/Page-2/navbar/Navbar";

class Views extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      currentScreen: 0,
      s: true,
      c: 0,
      styles: [],
      activeClass: "Homepage"
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    window.addEventListener("keydown", this.handleKeyPress.bind(this));
    var route = window.location.pathname;
    var routes = [
      "/",
      "/What-We-Provide",
      "/What-We-Do",
      "/About-Us",
      "/Our-Team",
      "/Contact"
    ];
    var pageid = routes.indexOf(route);
    // debugger;
    if (pageid === -1) pageid = route.includes("/What-We-Provide") ? 1 : -1;
    if (pageid === -1) pageid = route.includes("/What-We-Do") ? 2 : -1;
    //window.addEventListener("onPress", this.onPress.bind(this));
    // window.addEventListener("wheel", this.onWheel.bind(this));
    //document.addEventListener("wheel", this.wheelScroll.bind(this));
    window.onpopstate = e => {
      {
        console.log("popstate", e);
        pageid = routes.indexOf(e.state);
        if (pageid === -1) {
          window.location.pathname = "/";
        }
      }
    };
    if (pageid === -1) {
      // window.location.pathname = "/";

      var styles = [];
      styles[0] = { transform: "translateX(0px)", display: "block" };
      for (var i = 1; i <= 5; i++) {
        styles[i] = {
          transform: "translateX(+" + window.innerWidth + "px)",
          overflow: "hidden"
        };
      }
      this.setState({ styles: styles, currentScreen: 0 });
      this.setPageTitle(0);
    } else {
      console.log(pageid);
      var styles = [];

      for (var i = 0; i <= 5; i++) {
        styles[i] = {
          transform: "translateX(+" + window.innerWidth + "px)",
          overflow: "hidden"
        };
      }
      styles[pageid] = { transform: "translateX(0px)", display: "block" };
      this.setState({ styles: styles, currentScreen: pageid });
      this.setPageTitle(pageid);
    }
  }

  componentDidUpdate() {
    window.scrollTo(0, 0);
    //window.location.hash = "";
  }
  // here onclick of anchor the target page goes.
  setScreenNext = id => {
    let styles = [];
    styles = this.state.styles;
    styles[this.state.currentScreen] = {
      transform: "translateX(" + window.innerWidth + "px)",
      // transition: "transform 0.5s linear",
      display: "none"
    };
    styles[id] = {
      transform: "translateX(0px)",
      // transition: "transform 0.5s linear",
      display: "block"
    };
    this.setPageTitle(id);
    this.setState(
      {
        styles: styles,
        currentScreen: id
      },
      () => {
        $(".s" + (this.state.currentScreen + 1)).hide();
        $(".s" + (this.state.currentScreen + 1)).fadeIn(2000);
      }
    );
  };

  // here onclick of button next page goes to next page
  setScreen = id => {
    let styles = [];
    let that = this;
    let currentScreen = 0;

    if (this.state.currentScreen === 5) return;
    // var wheelHeight = $(document).height();
    // var wheelPosition = $(window).height() + $(window).scrollTop();
    // if (wheelHeight - wheelPosition <= 5) {
    currentScreen = (that.state.currentScreen + 1) % 6;
    styles = that.state.styles;
    styles[that.state.currentScreen] = {
      transform: "translateX(+" + window.innerWidth + "px)",
      display: "none"
    }; // transition: "transform 1s linear",
    styles[currentScreen] = {
      transform: "translateX(0px)",
      display: "block"
    }; // transition: "transform 1s linear",
    this.setPageTitle(currentScreen);
    that.setState(
      {
        currentScreen: currentScreen,
        styles: styles
      },
      () => {
        $(".s" + (that.state.currentScreen + 1)).hide();
        $(".s" + (that.state.currentScreen + 1)).fadeIn(2000);
      }
    );
    // }
  };
  // here onclick of button prev page goes to prev page
  setScreenPrev = id => {
    let styles = [];
    let that = this;
    let currentScreen = 0;

    //if (this.state.currentScreen === 0) return;
    //if (window.scrollY === 0) {
    currentScreen = ((that.state.currentScreen - 1) % 6 + 6) % 6;
    styles = that.state.styles;
    styles[that.state.currentScreen] = {
      transform: "translateX(" + window.innerWidth + "px)", // transition: "transform 0.5s linear",
      display: "none"
    };
    styles[currentScreen] = {
      transform: "translateX(0px)", // transition: "transform 0.5s linear",
      display: "block"
    };
    this.setPageTitle(currentScreen);
    that.setState({ currentScreen: currentScreen, styles: styles }, () => {
      $(".s" + (that.state.currentScreen + 1)).hide();
      $(".s" + (that.state.currentScreen + 1)).fadeIn(3000);
    });
    //}
  };
  // here onclick of down and up key functionality is there same as scroll
  handleKeyPress(e) {
    let that = this;
    let currentScreen = 0;
    let styles = [];

    if (window.innerWidth <= 768) {
      // return;
    }

    switch (e.keyCode) {
      case 38: {
        //if (this.state.currentScreen === 0) return;
        //if (window.scrollY === 0) {
        currentScreen = ((that.state.currentScreen - 1) % 6 + 6) % 6;
        styles = that.state.styles;
        styles[that.state.currentScreen] = {
          transform: "translateX(-" + window.innerWidth + "px)",
          // transition: "transform 0.5s linear",
          display: "none"
        };
        styles[currentScreen] = {
          transform: "translateX(0px)",
          // transition: "transform 0.5s linear",
          dsplay: "block"
        };
        //this.setPageTitle(currentScreen);
        //that.setState({
        //currentScreen: currentScreen,
        //styles: styles
        //});
        //}
        break;
      }
      case 40: {
        //if (this.state.currentScreen === 5) return;
        //var wheelHeight = $(document).height();
        //var wheelPosition = $(window).height() + $(window).scrollTop();
        //if (wheelHeight - wheelPosition <= 5) {
        //e.preventDefault();
        currentScreen = (that.state.currentScreen + 1) % 6;
        styles = that.state.styles;
        styles[that.state.currentScreen] = {
          transform: "translateX(+" + window.innerWidth + "px)",
          // transition: "transform 1s linear",
          display: "none"
        };
        styles[currentScreen] = {
          transform: "translateX(0px)",
          // transition: "transform 1s linear",
          display: "block"
        };
        // this.setPageTitle(currentScreen);
        // that.setState({
        //   currentScreen: currentScreen,
        //   styles: styles
        // });
        //}
        break;
      }
      default:
        console.log();
    }
  }
  // here Page title and url is getting set here.
  setPageTitle(screen) {
    switch (screen) {
      case 0:
        document.title = "Armentum";
        window.history.pushState("", "", "");
        break;
      case 1:
        document.title = "What We Provide | Armentum";
        window.history.pushState("", "what-we-provide", "what-we-provide");
        break;
      case 2:
        document.title = "How We Work | Armentum";
        window.history.pushState("", "how-we-work", "what-we-work");
        break;
      case 3:
        document.title = "About Us | Armentum";
        window.history.pushState("", "about-Us", "about-Us");
        break;
      case 4:
        document.title = "Our Team | Armentum";
        window.history.pushState("", "our-team", "our-team");
        break;
      case 5:
        document.title = "Contact | Armentum";
        window.history.pushState("", "contact", "contact");
        break;
      default:
        document.title = "Armentum";
    }
  }

  // stopLoading = () => {
  //   this.setState({ loading: false });
  // };

  componentWillMount() {
    //this.stopLoading();
  }

  handleNextClick = () => {
    let index = this.props.currentScreen;
    // this.setState({ loading: true });
    // setTimeout(
    //   function(that) {
    //     that.setState({ loading: false });
    //   },
    //   800,
    //   this
    // );
    this.setScreen(index);
  };

  handlePrevClick = () => {
    let index = this.props.currentScreen;
    // this.setState({ loading: true });
    // setTimeout(
    //   function(that) {
    //     that.setState({ loading: false });
    //   },
    //   800,
    //   this
    // );
    this.setScreenPrev(index);
  };

  render() {
    let styles = this.state.styles;
    const { loading } = this.state;
    let currentScreen = this.state.currentScreen;
    // if (loading){
    //   return <LoadingSpinner currentScreen={this.state.currentScreen} />;
    // }
    let CustomNavBar = <Navbar setScreenNext={this.setScreenNext} />;
    let CustomButton = (
      <Button
        preClick={this.handlePrevClick}
        nextClick={this.handleNextClick}
        currentScreen={this.state.currentScreen}
      />
    );

    if (currentScreen === 3 || currentScreen === 4 || currentScreen === 5) {
      CustomNavBar = <Navbar_2 setScreenNext={this.setScreenNext} />;
      CustomButton = (
        <Button
          color="black"
          preClick={this.handlePrevClick}
          nextClick={this.handleNextClick}
          currentScreen={this.state.currentScreen}
        />
      );
    }

    return (
      <div
        className="animate w3-animate-fading"
        style={{
          height: window.innerWidth > 768 ? window.innerHeight : "auto",
          background: "none "
        }}
      >
        <div className="item s1 active" style={styles[0]}>
          <Homepage setScreenNext={this.setScreenNext} />
        </div>
        <div className="item s2" style={styles[1]}>
          <div className="div-navbar">
            <WeProvide setScreenNext={this.setScreenNext} />
          </div>
        </div>
        <div className="item s3" style={styles[2]}>
          <div className="div-navbar1">
            <Howwework setScreenNext={this.setScreenNext} />
          </div>
        </div>
        <div className="item s4" style={styles[3]}>
          <AboutUs setScreenNext={this.setScreenNext} />
        </div>
        <div className="item s5" style={styles[4]}>
          <OurTeam setScreenNext={this.setScreenNext} />
        </div>
        <div className="item s6" style={styles[5]}>
          <Contact />
        </div>
        {/* <Navbar setScreenNext={this.setScreenNext} /> */}
        {CustomNavBar}
        {/* {loading ? <LoadingSpinner /> */}:{CustomButton}
        <div
          style={{
            position: "fixed",
            top: 0,
            left: 0, // transform: "transition(2s,ease)",
            width: "100%",
            height: "100%",
            display: this.state.loading === true ? "block" : "none"
          }}
        >
          <img
            src="/Front-images/loader.gif"
            className="fa-spin"
            style={{
              height: 150,
              width: 150,
              position: "absolute",
              left: "50%",
              top: "50%",
              marginLeft: "-75px",
              marginTop: "-75px",
              display: "none"
            }}
          />
        </div>
      </div>
    );
  }
}
export default Views;
//next and previous button
class Button extends React.Component {
  render() {
    return (
      <div
        className="button-bottom"
        style={{ position: "fixed", bottom: "10px", right: "100px" }}
      >
        <div className="next">
          <div className="previous">
            <button
              className="button2"
              onClick={this.props.preClick}
              style={{
                display:
                  this.props.currentScreen === 0 ? "none" : "inline-block",
                color: this.props.color
              }}
            >
              <span>
                {this.props.currentScreen === 3 ||
                this.props.currentScreen === 4 ||
                this.props.currentScreen === 5 ? (
                  <img
                    src="/Front-images/icon-arrow-left-blk.png"
                    className="arrow-image"
                  />
                ) : (
                  <img
                    src="/Front-images/icon-arrow-left.png"
                    className="arrow-image"
                  />
                )}
              </span>
              <span className="next-btn" style={{ marginLeft: "5px" }}>
                PREV
              </span>
            </button>
          </div>
          <button
            className="button1"
            onClick={this.props.nextClick}
            style={{
              display: this.props.currentScreen === 5 ? "none" : "inline-block",
              color: this.props.color
            }}
          >
            <span className="next-btn" style={{ marginRight: "5px" }}>
              NEXT
            </span>
            <span>
              {this.props.currentScreen === 3 ||
              this.props.currentScreen === 4 ||
              this.props.currentScreen === 5 ? (
                <img
                  src="/Front-images/icon-arrow-right-blk.png"
                  className="arrow-image"
                />
              ) : (
                <img
                  src="/Front-images/icon-arrow-right.png"
                  className="arrow-image"
                />
              )}
            </span>
          </button>
        </div>
      </div>
    );
  }
}

//     window.addEventListener("scroll", this.hightlightState.bind(this));
//     const style = this.props.style ? this.props.style : {};
//     const hashlinks = this.props.hashlinks ? this.props.hashlinks : [];
//     this.setState({
//       style: style,
//       hashlinks: hashlinks,
//       currentScreen: 0,
//       currentHash: 0
//     });
//   }

//   componentWillReceiveProps(prop) {
//     // console.log("Current screen props : ", this.props.currentScreen, prop.currentScreen);
//     var ulstyle = [
//       { display: "none" },
//       { display: "none" },
//       {},
//       { display: "none" }
//     ];
//     var listyle = [
//       { height: "10px" },
//       { height: "10px" },
//       {},
//       { height: "10px" }
//     ];
//     if (prop.currentScreen === 1) {
//       ulstyle = [
//         { display: "flex" },
//         { display: "none" },
//         {},
//         { display: "none" }
//       ];
//       listyle = [{ height: "80%" }, { height: "10px" }, {}, { height: "10px" }];
//     } else if (prop.currentScreen === 2) {
//       ulstyle = [
//         { display: "none" },
//         { display: "flex" },
//         {},
//         { display: "none" }
//       ];
//       listyle = [{ height: "10px" }, { height: "80%" }, {}, { height: "10px" }];
//     } else if (prop.currentScreen === 4) {
//       ulstyle = [
//         { display: "none" },
//         { display: "none" },
//         {},
//         { display: "flex" }
//       ];
//       listyle = [{ height: "10px" }, { height: "10px" }, {}, { height: "80%" }];
//     } else {
//       ulstyle = [
//         { display: "none" },
//         { display: "none" },
//         {},
//         { display: "none" }
//       ];
//       listyle = [
//         { height: "10px" },
//         { height: "10px" },
//         {},
//         { height: "10px" }
//       ];
//     }
//     this.setState({
//       ulstyle: ulstyle,
//       listyle: listyle,
//       currentScreen: prop.currentScreen
//     });
//   }

//   hightlightState(e) {
//     var index = this.state.currentScreen;
//     if (index === 1 || index === 2 || index === 4) {
//       var t = $(window).scrollTop();
//       for (var i = 1; i < this.state.hashlinks[index].length; i++) {
//         var ot = $("#" + this.state.hashlinks[index][i]).offset().top;
//         if (ot + window.innerHeight > t && ot < t) {
//           this.setState({
//             currentHash: i
//           });
//         } else if (t < window.innerHeight) {
//           this.setState({
//             currentHash: 0
//           });
//         }
//       }
//     }
//   }

//   handleClick(s, e) {
//     if (e.currentTarget.hash) {
//       $("html, body").animate(
//         {
//           scrollTop: $(e.currentTarget.hash).offset().top + 30
//         },
//         1000
//       );
//     } else {
//       $("html, body").animate(
//         {
//           scrollTop: 0
//         },
//         1000
//       );
//     }
//     this.setState({
//       currentHash: s
//     });
//   }

//   render() {
//     let hashlinks = this.props.hashlinks;
//     return (
//       <div className="right_new_navbar" style={this.state.style}>
//         <div style={{ paddingTop: 25 }}>
//           <img src="/Front-images/nav-icon.png" alt="Nav Icon" />
//         </div>
//         {/* <ul className="link-list">
//           {hashlinks &&
//             hashlinks.map((val, index) => {
//               var arr = [];
//               if (Array.isArray(val)) {
//                 var submenu = val.map((ele, i) => {
//                   return (
//                     <li
//                       key={i}
//                       className={
//                         "li-item " +
//                         (this.state.currentHash === i ? "link-active" : "")
//                       }
//                     >
//                       <a
//                         href={"#" + ele}
//                         className="link-item"
//                         onClick={this.handleClick.bind(this, i)}
//                       >

//                       </a>
//                     </li>
//                   );
//                 });
//                 arr.push(
//                   <li
//                     key="index"
//                     style={this.state.listyle[index - 1]}
//                     className="link-list-item"
//                   >
//                     <a
//                       className={
//                         "link-item sub-item " +
//                         (this.state.currentScreen === index
//                           ? "active-screen"
//                           : "")
//                       }
//                       onClick={() => {
//                         this.props.setScreen(index);
//                       }}
//                     >
//                       <ul
//                         className="submenu"
//                         style={this.state.ulstyle[index - 1]}
//                         key={index}
//                       >
//                         {submenu}
//                       </ul>
//                     </a>
//                   </li>
//                 );
//               } else {
//                 arr.push(
//                   <li
//                     key={index}
//                     className={
//                       "link-list-item " +
//                       (this.state.currentScreen === index ? "link-active" : "")
//                     }
//                   >
//                     <a
//                       className="link-item"
//                       onClick={() => {
//                         this.props.setScreen(index);
//                       }}
//                     >
//                       .
//                     </a>
//                   </li>
//                 );
//               }
//               // arr.push(<li>Element</li>);
//               return arr;
//             })}
//         </ul> */}
//         <div>
//           <img
//             src="/images/mouse-scroll.png"
//             alt="Mouse"
//             style={{ marginLeft: 16 }}
//           />
//         </div>
//       </div>
//     );
//   }
// }

// wheelScroll(e) {
//   if ($(this).scrollTop() > 1) {
//     $(".header").addClass("sticky");
//   } else {
//     $(".header").removeClass("sticky");
//   }

//  }
//   var wheelHeight = $(document).height();
//   var wheelPosition = $(window).height() + $(window).scrollTop();
//   if (wheelHeight - wheelPosition <= 5) {
//     if (this.state.s) {
//       this.setState({ s: false });
//       setTimeout(
//         function(that) {
//           that.setState({ s: true });
//         },
//         500,
//         this
//       );
//       if (e.deltaY > 0) {
//         e.preventDefault();
//         console.log("Scroll ------------> ", e.defaultPrevented, e);
//         window.scrollTo(0,0);
//         if (this.state.currentScreen === 5) return;
//         currentScreen = (that.state.currentScreen + 1) % 6;
//         styles = that.state.styles;
//         styles[that.state.currentScreen] = {
//           transform: "translateX(+" + window.innerWidth + "px)",
//           // transition: "transform 1s linear",
//           display: "none"
//         };
//         styles[currentScreen] = {
//           transform: "translateX(0px)",
//           // transition: "transform 1s linear",
//           display: "block"
//         };
//         this.setPageTitle(currentScreen);
//         that.setState({
//           currentScreen: currentScreen,
//           styles: styles
//         });
//       } else {
//         // currentScreen = ((that.state.currentScreen - 1) % 6 + 6) % 6;
//         // styles = that.state.styles;
//         // styles[that.state.currentScreen] = {
//         //   transform: "translateX(-"+ window.innerWidth +"px)",transition:"transform 1s linear", display: 'none'
//         // };
//         // styles[currentScreen] = {
//         //   transform: "translateX(0px)",transition:"transform 1s linear", display: 'block'
//         // };
//          // this.setPageTitle(currentScreen);
//         // that.setState({
//         //   currentScreen: currentScreen,
//         //   styles: styles
//         // });
//       }
//     } else {
//     }
//   }
//   // if ($(window).scrollTop() <= 0) {
//   //   if (this.state.s) {
//   //     this.setState({ s: false });
//   //     setTimeout(
//   //       function(that) {
//   //         that.setState({ s: true });
//   //       },
//   //       500,
//   //       this
//   //     );
//   //     if (e.deltaY < 0) {
//   //       e.preventDefault();
//   //       currentScreen = ((that.state.currentScreen - 1) % 6 + 6) % 6;
//   //       styles = that.state.styles;
//   //       styles[that.state.currentScreen] = {
//   //         transform: "translateX(-" + window.innerWidth + "px)",
//   //         transition: "transform 1s linear",
//   //         display: "none"
//   //       };
//   //       styles[currentScreen] = {
//   //         transform: "translateX(0px)",
//   //         transition: "transform 1s linear",
//   //         display: "block"
//   //       };
//   //       this.setPageTitle(currentScreen);
//   //       that.setState({
//   //         currentScreen: currentScreen,
//   //         styles: styles
//   //       });
//   //     }
//   //   }
//   // }
// }

// onPress(e){
//   let that = this;
//   let currentScreen = 0;
//   let styles = [];

//   if (this.state.currentScreen === 5) return;
//   var wheelHeight = $(document).height();
//   var wheelPosition = $(window).height() + $(window).scrollTop();
//   if (wheelHeight - wheelPosition <= 5) {
//     e.preventDefault();
//     currentScreen = (that.state.currentScreen + 1) % 6;
//     styles = that.state.styles;
//     styles[that.state.currentScreen] = { transform: "translateX(+" + window.innerWidth + "px)", // transition: "transform 1s linear",
//       display: "none" };
//     styles[currentScreen] = { transform: "translateX(0px)", // transition: "transform 1s linear",
//       display: "block" };
//     this.setPageTitle(currentScreen);
//     that.setState({ currentScreen: currentScreen, styles: styles });
//   }
// }

//     var styles = [];
//     styles[0] = { transform: "translateX(0px)", display: "block" };
//     for (var i = 1; i <= 5; i++) {
//       styles[i] = {
//         transform: "translateX(+" + window.innerWidth + "px)",
//         overflow: "hidden"
//       };
//     }
//     this.setState({ styles: styles, currentScreen: 0 });
//     this.setPageTitle(0);
//   } else {
//     console.log(pageid);
//     var styles = [];

//     for (var i = 0; i <= 5; i++) {
//       styles[i] = {
//         transform: "translateX(+" + window.innerWidth + "px)",
//         overflow: "hidden"
//       };
//     }
//     styles[pageid] = { transform: "translateX(0px)", display: "block" };
//     this.setState({ styles: styles, currentScreen: pageid });
//     this.setPageTitle(pageid);
//   }
