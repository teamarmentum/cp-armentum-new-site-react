import React, { Component } from "react";
// import { BrowserRouter as Router, Route } from "react-router-dom";
import "./index.css";
import Homepage from "./views/homepage/index";
import WeProvide from "./views/weprovide/index";
import AboutUs from "./views/aboutus/index";
import Contact from "./views/contact/index";
// import OurTeam from "./views/team/index";
// import MeetTeam from "./views/team/meetTeam";
// import JoinTeam from "./views/team/joinTeam";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentScreen: 0,
      styles: []
    };
  }
  componentDidMount() {
    window.addEventListener("keydown", this.handleKeyPress.bind(this));
    var styles = [];
    styles[0] = {
      transform: "translateX(0px)"
    };
    styles[1] = {
      transform: "translateX(4000px)"
    };
    styles[2] = {
      transform: "translateX(4000px)"
    };
    styles[3] = {
      transform: "translateX(4000px)"
    };
    this.setState({ styles: styles });
  }
  handleKeyPress(e) {
    let that = this;
    let currentScreen = 0;
    let styles = [];
    debugger;
    switch (e.keyCode) {
      /*case 37: {
        currentScreen = ((that.state.currentScreen - 1) % 4 + 4) % 4;
        styles = that.state.styles;
        styles[that.state.currentScreen] = {
          transform: "translateX(-4000px)"
        };
        styles[currentScreen] = {
          transform: "translateX(0px)"
        };
        that.setState({
          currentScreen: currentScreen,
          styles: styles
        });
        break;
      }*/
      case 38: {
        currentScreen = (that.state.currentScreen + 1) % 4;
        styles = that.state.styles;
        styles[that.state.currentScreen] = {
          transform: "translateX(-4000px)"
        };
        styles[currentScreen] = {
          transform: "translateX(0px)"
        };
        that.setState({
          currentScreen: currentScreen,
          styles: styles
        });
        break;
      }
      /*case 39: {
        currentScreen = (that.state.currentScreen + 1) % 4;
        styles = that.state.styles;
        styles[that.state.currentScreen] = {
          transform: "translateX(-4000px)"
        };
        styles[currentScreen] = {
          transform: "translateX(0px)"
        };
        that.setState({
          currentScreen: currentScreen,
          styles: styles
        });
        break;
      }*/
      case 40: {
        currentScreen = ((that.state.currentScreen - 1) % 4 + 4) % 4;
        styles = that.state.styles;
        styles[that.state.currentScreen] = {
          transform: "translateX(4000px)"
        };
        styles[currentScreen] = {
          transform: "translateX(0px)"
        };
        that.setState({
          currentScreen: currentScreen,
          styles: styles
        });
        break;
      }
      default:
        currentScreen = this.state.currentScreen;
    }
  }
  render() {
    let styles = this.state.styles;
    return (
      <div>
        {/* <Router>
          <div>
            <Route exact path="/" component={Homepage} />
            <Route path="/what-we-do" component={WeProvide} />
            <Route path="/about-us" component={AboutUs} />
            <Route path="/contact" component={Contact} />
            <Route path="/team" component={OurTeam} />
            <Route path="/meet-team" component={MeetTeam} />
            <Route path="/join-team" component={JoinTeam} />
          </div>
        </Router> */}
        {/* <div className="slider"> */}
        <div className="item s1" style={styles[0]}>
          <Homepage />
        </div>
        <div className="item s2" style={styles[1]}>
          <WeProvide />
        </div>
        <div className="item s3" style={styles[2]}>
          <AboutUs />
        </div>
        <div className="item s4" style={styles[3]}>
          <Contact />
        </div>
        {/* </div> */}
      </div>
    );
  }
}

export default App;
