export const testimonials = [
  {
    data:
      "Armentum provides GREAT value. They provide reasonable pricing, stick to their quotes, and never nickel and dime. I've done a ton of development with them and aside from the great value they provide, I've really enjoyed their design team and management technology to help make providing input and managing the process as easy as possible. ",
    by: "Jacob Blackett",
    designation: " "
  },
  {
    data:
      "Armentum came in on a rescue mission after we had to let our other agency go. Their team is pretty fast on responding and giving clear options with pros and cons. Kudos to Armentum Team for their hard work. ",
    by: "Ron Desmond, InterPlay LLC",
    designation: "Chief Technology Officer"
  },
  {
    data:
      "Working with Armentum is one of the best agency relationships that we have had because of their strong technical expertise especially in Devops. Happy that they were able to handle the large traffic we get on our web app",
    by: "Natalie McNeil, Fortune 1000 Company",
    designation: "Product Manager"
  },
  {
    data:
      "From creating the Roadmap to Support, Ameet’s Armentum team has been transparent and prompt in addressing roadblocks and also keeping our team in loop. This helped in overcoming obstacles and also streamlining our marketing strategy.We have worked with them for over 18 months now and continue to do so. ",
    by: "Rico Alves, Redeal Inc",
    designation: "Chief Operationg Officer "
  },
  {
    data:
      "We came to Armentum on a recommendation from another friend who runs a mid-sized e-commerce site. Armentum helped us increase our conversions by running A/B tests and expertiments.  ",
    by: "Max Harper, Mimesis Inc",
    designation: "Group Marketing Manager"
  }
];
